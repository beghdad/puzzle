package projet;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;

public class View extends JFrame {
	private int height =800;
	private int width = 800;
	private Controler controler;
	private BufferedImage im;
	private Window w;
	private ImageWindow iw;
	Board b;
	
	public View(Controler controler){
		this.controler=controler;
		setSize(width,height);
		setLocationRelativeTo(null);
		
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
		setLayout(new BorderLayout());
		setJMenuBar(createMenu());
		add(createToolBar(),BorderLayout.NORTH);
	}
	
	public void setBoard(BufferedImage image){
		im=image;
		b= new Board(10,10,image);
		b.creerPieces();
		b.setBounds(0, 0, 800, 800);
		for(int i=0;i<b.getListe().size();i++){
			add(b.getListe().get(i),BorderLayout.CENTER);
			b.getListe().get(i).setBounds((int)(Math.random()*300),(int)(Math.random()*300), b.getListe().get(i).getWidth(), b.getListe().get(i).getWidth());
	
		}
		b.setImage();
		b.repaint();
	}
	
		
	
	// creation de la bar de menu
	public JMenuBar createMenu(){
		JMenuBar menuBar = new JMenuBar();
		JMenu fichier= new JMenu("Fichier");
		fichier.setMnemonic('F');
		JMenu edit = new JMenu("Edit");
		edit.setMnemonic('E');
		JMenu aide = new JMenu("Aide");
		menuBar.add(fichier);
		menuBar.add(edit);
		menuBar.add(aide);
		aide.setMnemonic('A');
		fichier.add(actOpen);
		fichier.add(actRestart);
		fichier.addSeparator();
		fichier.add(actSave);
		fichier.add(actSaveAs);
		fichier.addSeparator();
		fichier.add(actExit);
		edit.add(actUndo);
		edit.add(actRedo);
		aide.add(actAbout);
		return menuBar;
	}
	
	//creation de la toolbar
	public JToolBar createToolBar(){
		JToolBar toolBar = new JToolBar();
		toolBar.add(actOpen);
		toolBar.add(actRestart);
		toolBar.addSeparator();
		toolBar.add(actSave);
		toolBar.add(actSaveAs);
		toolBar.addSeparator();
		toolBar.add(actUndo);
		toolBar.add(actRedo);
		toolBar.addSeparator();
		toolBar.add(actExit);
		return toolBar;
		
	}
	
	
	// les action pour regrouper les trucs de menu et toolbar et popupmenu
	public AbstractAction actOpen = new AbstractAction(){
		{
			 putValue( Action.NAME, "Open" );
		     putValue( Action.SMALL_ICON, new ImageIcon( "icons/open.png" ) );
		     //putValue( Action.MNEMONIC_KEY, KeyEvent.VK_O );
		     putValue( Action.SHORT_DESCRIPTION, "Ouvrir (CTRL+O)" );       
		     putValue( Action.ACCELERATOR_KEY,KeyStroke.getKeyStroke(KeyEvent.VK_O, KeyEvent.CTRL_DOWN_MASK ) ); 
		}
		
		public void actionPerformed(ActionEvent arg0) {
			try {
				controler.open();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	};
	
	public AbstractAction actUndo = new AbstractAction(){
		{
			 putValue( Action.NAME, "Undo" );
		     putValue( Action.SMALL_ICON, new ImageIcon( "icons/undo.png" ) );
		    // putValue( Action.MNEMONIC_KEY, KeyEvent.VK_U );
		     putValue( Action.SHORT_DESCRIPTION, "Undo (CTRL+Z)" );       
		     putValue( Action.ACCELERATOR_KEY,KeyStroke.getKeyStroke(KeyEvent.VK_Z, KeyEvent.CTRL_DOWN_MASK ) ); 
		}
		
		public void actionPerformed(ActionEvent arg0) {
System.out.println("undo");			
		}
	};
	
	public AbstractAction actRedo= new AbstractAction(){
		{
			 putValue( Action.NAME, "Redo" );
		     putValue( Action.SMALL_ICON, new ImageIcon( "icons/redo.png" ) );
		    // putValue( Action.MNEMONIC_KEY, KeyEvent.VK_R );
		     putValue( Action.SHORT_DESCRIPTION, "Redo (CTRL+R)" );       
		     putValue( Action.ACCELERATOR_KEY,KeyStroke.getKeyStroke(KeyEvent.VK_R, KeyEvent.CTRL_DOWN_MASK ) ); 
		}
		
		public void actionPerformed(ActionEvent arg0) {
System.out.println("redo");			
		}
	};
	
	public AbstractAction actRestart = new AbstractAction(){
		{
			 putValue( Action.NAME, "Restart" );
		     putValue( Action.SMALL_ICON, new ImageIcon( "icons/new.png" ) );
		    // putValue( Action.MNEMONIC_KEY, KeyEvent.VK_F );
		     putValue( Action.SHORT_DESCRIPTION, "Restart F2" );       
		     putValue( Action.ACCELERATOR_KEY,KeyStroke.getKeyStroke((char)KeyEvent.VK_F2 ) ); 
		}
		
		public void actionPerformed(ActionEvent arg0) {
			System.out.println("restart");
			
		}
	};
	
	public AbstractAction actExit = new AbstractAction(){
		{
			 putValue( Action.NAME, "Exit" );
		     putValue( Action.SMALL_ICON, new ImageIcon( "icons/exit.png" ) );
		    // putValue( Action.MNEMONIC_KEY, KeyEvent.VK_E );
		     putValue( Action.SHORT_DESCRIPTION, "Exit (CTRL+Q)" );       
		     putValue( Action.ACCELERATOR_KEY,KeyStroke.getKeyStroke(KeyEvent.VK_Q, KeyEvent.CTRL_DOWN_MASK ) ); 
		}
		
		public void actionPerformed(ActionEvent arg0) {
			controler.exit();
		}
	};
	
	public AbstractAction actSave = new AbstractAction(){
		{
			 putValue( Action.NAME, "Save" );
		     putValue( Action.SMALL_ICON, new ImageIcon( "icons/save.png" ) );
		    // putValue( Action.MNEMONIC_KEY, KeyEvent.VK_S );
		     putValue( Action.SHORT_DESCRIPTION, "Save (CTRL+S)" );       
		     putValue( Action.ACCELERATOR_KEY,KeyStroke.getKeyStroke(KeyEvent.VK_S, KeyEvent.CTRL_DOWN_MASK ) ); 
		}
		
		public void actionPerformed(ActionEvent arg0) {
			System.out.println("save");
			
		}
	};
	
	public AbstractAction actSaveAs = new AbstractAction(){
		{
			 putValue( Action.NAME, "Save_as" );
		     putValue( Action.SMALL_ICON, new ImageIcon( "save_as/new.png" ) );
		    // putValue( Action.MNEMONIC_KEY, KeyEvent.VK_V );
		     putValue( Action.SHORT_DESCRIPTION, "Save_As (CTRL+V)" );       
		     putValue( Action.ACCELERATOR_KEY,KeyStroke.getKeyStroke(KeyEvent.VK_V, KeyEvent.CTRL_DOWN_MASK ) ); 
		}
		
		public void actionPerformed(ActionEvent arg0) {
			System.out.println("save as");
			
		}
	};
	
	public AbstractAction actAbout = new AbstractAction(){
		{
			 putValue( Action.NAME, "About" );
		     putValue( Action.SMALL_ICON, new ImageIcon( "about/new.png" ) );
		    // putValue( Action.MNEMONIC_KEY, KeyEvent.VK_A );
		     putValue( Action.SHORT_DESCRIPTION, "About (CTRL+A)" );       
		     putValue( Action.ACCELERATOR_KEY,KeyStroke.getKeyStroke(KeyEvent.VK_A, KeyEvent.CTRL_DOWN_MASK ) ); 
		}
		
		public void actionPerformed(ActionEvent arg0) {
			System.out.println("about");
			
		}
	};
	
	public void setImage(BufferedImage a){
		im=a;
	}
	public BufferedImage getImage(){
		return im;
	}
	
	public void creatWindow(){
		w=new Window(this);
		w.setVisible(true);
		w.setSize(300,200);
		iw=new ImageWindow(im);
		iw.repaint();
		w.add(iw);
		
	}
}
