package projet;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.HeadlessException;
import java.awt.Window;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

@SuppressWarnings("serial") 


public class ImageWindow extends JPanel{
	private BufferedImage im;
	
	public ImageWindow(BufferedImage a){
		im=a;
	}

	public void paintComponent(Graphics g){
		g.drawImage(im, 0,0,null);
	}
}
