package projet;

import java.awt.FileDialog;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Controler {
		private View view;
		private String fileName;
		private String fileAdresse;
		
		
		public Controler (){
			
		}
		public void setView(View view){
			this.view=view;
		}
		public void exit(){
			view.dispose();
		}
		public void open() throws IOException {
			FileDialog fd = new FileDialog(view,"ouvrir",FileDialog.LOAD);
			fd.setFilenameFilter(new FilenameFilter() {
			    
			    public boolean accept(File dir, String name) {
			        return name.endsWith(".png") || name.endsWith(".jpg") || name.endsWith(".gif") || name.endsWith(".jpeg") ;
			    }
			});
			fd.setVisible(true);
			if(fd.getFile()!=null){
				fileName=fd.getFile();
				fileAdresse=fd.getDirectory();

			}
			try{
				
			
			File f = new File(fileAdresse + fileName);
			BufferedImage im= ImageIO.read(f);
			view.setImage(im);
			view.creatWindow();
			view.setBoard(im);
			
			}
			catch( Exception e){
				System.out.println("erreur");
			}
		}
}
