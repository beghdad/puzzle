package projet;

import java.awt.Container;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;

import javax.swing.JComponent;
import javax.swing.JFrame;

public class Main {
  public static void main(String[] args) {
    JFrame jf = new JFrame("Demo");
    Container cp = jf.getContentPane();
    MyCanvas tl = new MyCanvas();
    cp.add(tl);
    jf.setSize(300, 200);
    jf.setVisible(true);
  }
}

class MyCanvas extends JComponent {

  public void paint(Graphics g) {
    Shape shape = new Rectangle2D.Float(100, 50, 80, 80);
    
    Graphics2D g2 = (Graphics2D) g;


   AffineTransform at = AffineTransform.getRotateInstance(Math.PI,140,50);   
   
   g2.setTransform(at);
    g2.draw(shape);
    
  }
}
