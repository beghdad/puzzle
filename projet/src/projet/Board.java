package projet;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.swing.JPanel;

public class Board extends JPanel{
	private int nbLigne;
	private int nbColonne;
	private BufferedImage image;
	private double width,height;
	private ArrayList<Piece> piece;
	public Board(int a,int b,BufferedImage image){
		setSize(800, 800);
		this.image=image;
		piece = new ArrayList<Piece>();
		if (a%2==1)
			nbLigne=a+1;
		else
			nbLigne=a;
		if (b%2==1)
			nbColonne=b+1;
		else
			nbColonne=b;
		width=image.getWidth()/nbColonne;
		height=image.getHeight()/nbLigne;
	}
	
	public void creerPieces(){
		for(int i=0;i<nbLigne;i++){
			for(int j=0;j<nbColonne;j++){
				if(i==0){
					if(j==0){
						piece.add(new PieceTL(width,height,new Point(0,0)));
					}
					else{
						if(j==nbColonne-1){
							piece.add(new PieceTR(width,height, new Point(j*width,0)));
						}
						else{
							if(j%2!=0){
								piece.add(new PieceTopB(width,height, new Point(j*width,0)));
							}
							else{
								piece.add(new PieceTopA(width,height, new Point(j*width,0)));
							}
						}
					}
								
				}
				else{
					if(i==nbLigne-1){
						if(j==0){
							piece.add(new PieceBL(width,height, new Point(j*width,i*height)));
						}
						else{
							if(j==nbColonne-1)
								piece.add(new PieceBR(width,height, new Point(j*width,i*height)));
							else
								if(j%2!=0)
									piece.add(new PieceBotA(width,height, new Point(j*width,i*height)));
								else
									piece.add(new PieceBotB(width,height, new Point(j*width,i*height)));
						}
					}
					else{
						if(j==0){
							if(i%2==0)
								piece.add(new PieceLeftB(width,height, new Point(j*width,i*height)));
							else
								piece.add(new PieceLeftA(width,height, new Point(j*width,i*height)));
						}	
						else{
							if(j==nbLigne-1){
								if(i%2==0)
									piece.add(new PieceRightA(width,height, new Point(j*width,i*height)));
								else
									piece.add(new PieceRightB(width,height, new Point(j*width,i*height)));
							}
							else{
								if((i+j)%2==0)
									piece.add(new PieceCentreB(width,height, new Point(j*width,i*height)));
								else
									piece.add(new PieceCentreA(width,height, new Point(j*width,i*height)));
							}
						}
					}
				}
			}
		}
	}
	
	public ArrayList<Piece> getListe(){
		return piece;
	}
	public void paintComponent(Graphics g){
		for(int i=0;i<piece.size();i++){
			piece.get(i).repaint();
		}
		
	}
	public void setImage(){

		for(int i=0;i<piece.size();i++){
			piece.get(i).setImage(image);
		}
		repaint();
	}
}
