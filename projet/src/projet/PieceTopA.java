package projet;

import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;

public class PieceTopA extends Piece{

	public PieceTopA(double width,double height,Point p){
		super(width,height,p);
		setPath2D();
		shape=getShape();
	
	}

	@Override
	public void setPath2D() {
		Path2D.Double p=new Path2D.Double();
		p.moveTo(point.getX(), point.getY());
		p.lineTo(point.getX()+width,point.getY());
		p.append(dentRight(), true);
		p.append(creuBot(), true);
		p.append(dentLeft(), true);
		path=p;
	}
	
	public Shape getShape(){
		return path.createTransformedShape(AffineTransform.getTranslateInstance(0, 0));
	}
}
