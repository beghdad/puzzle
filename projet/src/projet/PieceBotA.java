package projet;

import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;

public class PieceBotA extends Piece{

	public PieceBotA(double  width,double height,Point p){
		super(width,height,p);
		setPath2D();
		shape=getShape();
	
	}

	@Override
	public void setPath2D() {
		Path2D.Double p=new Path2D.Double();
		p.moveTo(point.getX(), point.getY());
		p.append(creuTop(),true);
		p.append(dentRight(), true);		
		p.lineTo(point.getX(), point.getY()+height);
		p.append(dentLeft(), true);
		
		path=p;
	}
	
	public Shape getShape(){
		return path.createTransformedShape(AffineTransform.getTranslateInstance(0, 0));
	}
}