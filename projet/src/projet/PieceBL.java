package projet;

import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;

public class PieceBL extends Piece{

	public PieceBL(double width,double height,Point p){
		super(width,height,p);
		setPath2D();
		shape=getShape();
	
	}

	@Override
	public void setPath2D() {
		Path2D.Double p=new Path2D.Double();
		p.moveTo(point.getX(), point.getY());
		p.lineTo(point.getX(),point.getY()+height);
		p.lineTo(point.getX()+width, point.getY()+height);
		p.moveTo(point.getX()+width, point.getY()+height);
		p.closePath();
		p.append(creuRight(), true);
		p.append(dentTop(),true);
		path=p;
	}
	
	public Shape getShape(){
		return path.createTransformedShape(AffineTransform.getTranslateInstance(0, 0));
	}
}