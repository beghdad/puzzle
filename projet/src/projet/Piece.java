package projet;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

public abstract  class Piece extends JPanel{
	protected Shape shape;
	protected Path2D.Double px,py,path;
	BufferedImage a ;
	protected BufferedImage image;
	protected double width,height,x,y;
	protected Point point;
	public Piece(double width ,double height,Point p){
		this.width=width;
		this.height=height;
		setSize(new Dimension((int)width,(int)height));
		point=p;
		this.px=new Path2D.Double();
		this.px.moveTo(p.getX(), p.getY());

		this.py=new Path2D.Double();
		this.py.moveTo(p.getX(), p.getY());
		creerPathX(p.getX(),p.getY());
		creerPathY(p.getX(),p.getY());
		x=p.getX();
		y=p.getY();
		setOpaque(false);
		
	}
	public abstract void setPath2D();
	
	public void paintComponent(Graphics g){
		Graphics2D g2= (Graphics2D) g;
		
		g2.setClip(shape);
		g2.drawImage(image,0,0,null);
		//g2.fillRect(00, 00, 1000, 1000);
		g2.setColor(Color.red);
		g2.draw(shape);
		
	}
	public void creerPathX(double x,double y){
		px.moveTo(point.getX(), point.getY());
		px.lineTo(x+width/5,y);
		px.curveTo(x+width*34/100,y,x+width*40/100,y+	height*8/100,x+width*43/100,y+ height*12/100);
		px.curveTo(x+width*36/100,y+height*16/100,x+width*36/100,y+	height*26/100,x+width*36/100,y+ height*34/100);
		px.curveTo(x+width*42/100,y+height*40/100,x+width*50/100,y+	height*40/100,x+width*58/100, y+height*40/100);
		px.curveTo(x+width*64/100,y+height*34/100,x+width*64/100,y+	height*26/100,x+width*64/100, y+height*16/100);
		px.curveTo(x+width*57/100,y+height*12/100,x+width*60/100,y+	height*8/100,x+width*66/100, y);
		px.lineTo(x+width*4/5, y);
		px.lineTo(x+width,y);
	}
	
	public void creerPathY(double x,double y){
		py.moveTo(point.getX(), point.getY());
		py.lineTo(x,y+height/5);
		py.curveTo(x,y+width*34/100,x+width*8/100,y+	height*40/100,x+width*12/100,y+ height*43/100);
		py.curveTo(x+width*16/100,y+height*36/100,x+width*26/100,y+	height*36/100,x+width*34/100,y+ height*36/100);
		py.curveTo(x+width*40/100,y+height*42/100,x+width*40/100,y+	height*50/100,x+width*40/100, y+height*58/100);
		py.curveTo(x+width*34/100,y+height*64/100,x+width*26/100,y+	height*64/100,x+width*16/100, y+height*64/100);
		py.curveTo(x+width*12/100,y+height*57/100,x+width*8/100,y+	height*60/100,x, y+height*66/100);
		py.lineTo(x, y+height*4/5);
		py.lineTo(x,y+height);
	}
	public Shape creuTop(){
		
		return px.createTransformedShape(new AffineTransform(1,0,0,1,0,0));
		}
	

	public Shape creuLeft(){
		AffineTransform at = new AffineTransform(1,0,0,1,0,0);
		
		return py.createTransformedShape(at);
		}
	

	public Shape creuBot(){
		AffineTransform at = new AffineTransform();
		at.rotate(Math.PI,point.getX()+width/2,point.getY()+height/2);
		return px.createTransformedShape(at);
		}
	

	public Shape creuRight(){
		AffineTransform at = new AffineTransform();
		at.rotate(Math.PI,point.getX()+width/2,point.getY()+height/2);
		return py.createTransformedShape(at);
		}
	
	public Shape dentTop(){

		AffineTransform at = new AffineTransform();
		at.rotate(Math.PI,point.getX()+width/2,point.getY());
		return px.createTransformedShape(at);	
	}
	
	public Shape dentLeft(){
		AffineTransform at = new AffineTransform();
		at.rotate(Math.PI,point.getX(),point.getY()+height/2);
		return py.createTransformedShape(at);
	}
	
	public Shape dentBot(){
	AffineTransform at = new AffineTransform();
	at.translate(0, height);
	return px.createTransformedShape(at);
	}
	
	public Shape dentRight(){
		AffineTransform at = new AffineTransform();
		at.translate(width, 0);
		return py.createTransformedShape(at);
	}
	public void setImage(BufferedImage a){
		image=a;
	}
	
	
	

}
